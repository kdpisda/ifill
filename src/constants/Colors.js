import { PRIMARY_COLOR, ERROR_COLOR, PRIMARY_VARIENT_COLOR, SECONDARY_COLOR } from '../config';
const tintColor = SECONDARY_COLOR;

export default {
  tintColor,
  tabIconDefault: PRIMARY_COLOR,
  tabIconSelected: PRIMARY_VARIENT_COLOR,
  tabBar: PRIMARY_VARIENT_COLOR,
  errorBackground: ERROR_COLOR,
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
