import axios from "axios";
import { API_SERVER } from "../config";
import { get } from "./helpers";

export const getDevices = store => {
  const { auth, ui, device } = store;
  ui.isLoading = true;
  return axios
    .get(`${API_SERVER}/appprofile/device_list/`, {
      headers: {
        Authorization: `${auth.jwt}`,
        Accept: 'application/json'
      }
    })
    .then(response => {
      device.fetchDeviceList = false;
      device.deviceList = get(["devices"])(response.data);
      device.length = get(["length"])(response.data);
      ui.successMessage = "Fetched";
      ui.isSuccess = true;
    })
    .catch(e => {
      device.fetchDeviceList = false;
      ui.errorMessage = get(["response", "data", "message"])(e) || e.message;
      ui.isError = true;
    })
    .finally(() => {
      ui.isLoading = false;
    });
};

export const getRealTimeDevices = store => {
    const { auth, ui, device } = store;
    ui.isLoading = true;
    return axios
      .get(`${API_SERVER}/appprofile/live_devices/`,{
          headers: {
            Authorization: `${auth.jwt}`,
            Accept: 'application/json'
          }
      })
      .then(response => {
        device.live = get(["live"])(response.data);
        ui.successMessage = "Fetched";
        ui.isSuccess = true;
        return true;
      })
      .catch(e => {
        device.fetchDeviceList = false;
        ui.errorMessage = get(["response", "data", "message"])(e) || e.message;
        ui.isError = true;
        return false;
      })
      .finally(() => {
        ui.isLoading = false;
        return false;
      });
};

export const requestToToggle = (store, deviceId) => {
    const { auth, ui } = store;
    ui.isLoading = true;
    return axios
      .post(`${API_SERVER}/appprofile/request_toggle/`,{
          headers: {
            Authorization: `${auth.jwt}`,
            Accept: 'application/json'
          },
          deviceId
      })
      .then(response => {
        ui.successMessage = "Fetched";
        ui.isSuccess = true;
        return get(["success"])(response.data);        
      })
      .catch(e => {
        device.fetchDeviceList = false;
        ui.errorMessage = get(["response", "data", "message"])(e) || e.message;
        ui.isError = true;
        return false;
      })
      .finally(() => {
        ui.isLoading = false;
        return false;
      });
}