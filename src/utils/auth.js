import axios from "axios";
import { API_SERVER } from "../config";
import { get } from "./helpers";

export const signIn = (store, username, password) => {
  const { auth, ui } = store;
  ui.isLoading = true;
  return axios
    .post(`${API_SERVER}/appprofile/applogin/`, {
      username,
      password
    })
    .then(response => {
      auth.jwt = get(["token"])(response.data);
      console.log(response.data);
      ui.isLoading = false;
      return true;
    })
    .catch(e => {
      ui.isError = true;
      ui.isLoading = false;
      ui.errorMessage = e.response;
      return false;
    });
};

export const signUp = (store, params) => {
  const { auth, ui } = store;
  ui.isLoading = true;
  return axios
    .post(`${API_SERVER}/auth/local/register`, params)
    .then(response => {
      auth.jwt = get(["token"])(response);
      ui.isLoading = false;
      return true;
    })
    .catch(e => {
      ui.isError = true;
      ui.isLoading = false;
      ui.errorMessage = e.errorMessage;
      return false;
    });
};
