import { createStackNavigator, createSwitchNavigator } from "react-navigation";
import LoginScreen from "../screens/LoginScreen";
import SignupScreen from "../screens/SignupScreen";
import DeviceScreen from "../screens/devices";
import DeviceInfoScreen from "../screens/devices/info";
import MainTabNavigator from "./MainTabNavigator";

const LoginStack = createStackNavigator({
  Login: LoginScreen
});

const SignupStack = createStackNavigator({
  Signup: SignupScreen
});

const DeviceStack = createStackNavigator({
  Device: DeviceScreen
});

const DeviceInfoStack = createStackNavigator({
  DeviceInfo: DeviceInfoScreen
});

export default createSwitchNavigator(
  {
    Main: MainTabNavigator,
    Login: LoginStack,
    Signup: SignupStack,
    Device: DeviceStack,
    DeviceInfo: DeviceInfoStack
  },
  {
    initialRouteName: "Login"
  }
);
