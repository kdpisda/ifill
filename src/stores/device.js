import { observable } from "mobx";

export default class DeviceStore {
  @observable
  fetchDeviceList = true;

  @observable
  deviceList = [];

  @observable
  length = 0;

  @observable
  live = 0;
}
