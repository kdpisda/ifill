import { observable } from "mobx";

export default class UIStore {
  @observable
  fetchedFromPersist = false;

  @observable
  isError = false;

  @observable
  isSuccess = false;

  @observable
  isLoading = false;

  @observable
  errorMessage = "";

  @observable
  successMessage = "";

  @observable
  ui = "this is ui store";
}
