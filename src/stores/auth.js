import { observable } from "mobx";
import { persist } from "mobx-persist";

export default class AuthStore {
  @persist
  @observable
  jwt = "guesttoken";
}
