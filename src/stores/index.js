import { AsyncStorage } from "react-native";
import { create } from "mobx-persist";
import AuthStore from "./auth";
import UIStore from "./ui";
import DeviceStore from "./device";

class Store {
  constructor() {
    this.auth = new AuthStore(this);
    this.ui = new UIStore(this);
    this.device = new DeviceStore(this);
  }
}

const store = new Store();
export default store;

const hydrate = create({
  storage: AsyncStorage
});

const p1 = hydrate("jwt", store.auth);

Promise.all([p1]).then(() => {
  store.ui.fetchedFromPersist = true;
});
