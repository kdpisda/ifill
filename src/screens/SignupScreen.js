import React from 'react';
import { StyleSheet, Text, TextInput, View, KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import { Card, Button } from 'react-native-elements';
import { PRIMARY_COLOR, PRIMARY_VARIENT_COLOR, SECONDARY_COLOR, BACKGROUND_COLOR, BODY_FONT } from '../config';

const styles = StyleSheet.create({
    container: {
        flex:1,
        width: '100%',
        height:'100%',
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: BACKGROUND_COLOR
    },
    loginCard:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputBox:{
        padding: 10,
        borderColor: "#ddd",
        borderWidth: 1,
        width: '100%',
        height: 50,
        margin: 5,
        alignSelf:'center',
        textAlign: 'center',
        fontFamily: BODY_FONT,
        fontSize: 20
    },
    button:{
        padding: 0,
        borderColor: "#ddd",
        borderWidth: 1,
        width: 275,
        height: 50,
        margin: 5,
        alignSelf:'center'
    },
    cardHeader:{
        fontFamily: BODY_FONT,
        fontSize: 18,
        textAlign: "center"
    },
    bottomText:{
        fontFamily: BODY_FONT
    },
    bottomTextLink:{
        fontFamily: BODY_FONT,
        textDecorationLine: 'underline'
    }
});

export default class SignupScreen extends React.Component{
    static navigationOptions = {
        header: null,
    };

    render(){
        return (
            <KeyboardAvoidingView
                behavior="padding"
                style={{
                    flex: 1,
                }}
            >
                <ScrollView>
                    <View style={styles.container}>
                        <Text>
                            {"\n"}
                        </Text>
                        <Image
                            style={{width:275, height:150}}
                            source={require('../assets/images/icon.png')}
                        />
                        <Card style={styles.loginCard}>
                            <View style={{width:275, alignSelf:'center', justifyContent:'center', borderRadius: 20}}>
                                <Text style={styles.cardHeader}>
                                    Signup {"\n"}
                                </Text>
                                <TextInput
                                    style={styles.inputBox}
                                    underlineColorAndroid="transparent"
                                    placeholder="Email"
                                />
                                <TextInput
                                    style={styles.inputBox}
                                    underlineColorAndroid="transparent"
                                    placeholder="First Name"
                                />
                                <TextInput
                                    style={styles.inputBox}
                                    underlineColorAndroid="transparent"
                                    placeholder="Last Name"
                                />
                                <TextInput
                                    style={styles.inputBox}
                                    underlineColorAndroid="transparent"
                                    placeholder="Contact Number"
                                />
                                <TextInput
                                    style={styles.inputBox}
                                    secureTextEntry
                                    underlineColorAndroid="transparent"
                                    placeholder="Password"
                                />
                                <Button
                                    backgroundColor='#000'
                                    buttonStyle={styles.button}
                                    title='Signup'
                                    fontFamily={BODY_FONT}
                                    fontSize={20}
                                />
                            </View>
                        </Card>
                        <Text>
                            {"\n"}
                        </Text>
                        <Text style={styles.bottomText}>
                            Already have an account ?
                        </Text>
                        <Text 
                            style={styles.bottomTextLink}
                            onPress={() => this.props.navigation.navigate('Login')}
                        >
                            Login {"\n"}
                        </Text>
                        <Text>
                            {"\n"}
                        </Text>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}