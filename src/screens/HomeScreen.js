import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl
} from "react-native";
import { Card, ListItem } from "react-native-elements";
import { Grid, Row, Col } from "react-native-easy-grid";
import { Entypo } from "@expo/vector-icons";
import { inject } from "mobx-react";
import { observer } from "mobx-react/native";
import {
  BODY_FONT,
  BACKGROUND_COLOR,
  PRIMARY_COLOR,
  PRIMARY_VARIENT_COLOR,
  TITLE_FONT,
  SECONDARY_COLOR,
  DEFAULT_TEXT_COLOR,
  FONT_SECONDARY_COLOR,
  ERROR_COLOR,
  FONT_ERROR_COLOR
} from "../config";
import { getDevices, getRealTimeDevices } from "../utils";

const schedulesList = [
  {
    start: "11:00",
    end: "12:00",
    day: "Monday"
  },
  {
    start: "18:00",
    end: "19:00",
    day: "Monday"
  }
];

const errorsList = [
  {
    name: "Unable to start",
    details: "Vice President",
    time: "12:00",
    day: "Monday"
  },
  {
    name: "Unable to start",
    details: "Vice President",
    time: "12:00",
    day: "Monday"
  }
];

class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require("../assets/images/icon_reverse.png")}
        style={{ width: 75, height: 35, justifyContent: "center", alignItems: "center" }}
      />
    );
  }
}

@inject("store")
@observer
class DeviceList extends React.Component {

  render() {
    const { store, navigation } = this.props;
    
    return (
      <View>
        {store.device.deviceList.map((item, i) => (
          <ListItem
            key={i}
            badge={{
              value: item.status == 1 ? "Active" : "Inactive",
              textStyle: {
                color: item.status ? FONT_SECONDARY_COLOR : FONT_ERROR_COLOR,
                fontFamily: BODY_FONT
              },
              containerStyle: {
                backgroundColor: item.status ? SECONDARY_COLOR : ERROR_COLOR
              }
            }}
            title={item.name}
            subtitle={item.serial_id}
            titleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
            subtitleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
          />
        ))}
    </View>
    );
  }
}

class ScheduleList extends React.Component {
  render() {
    return (
      <View>
        {schedulesList.map((item, i) => (
          <ListItem
            key={i}
            badge={{
              value: item.day,
              textStyle: {
                color: FONT_SECONDARY_COLOR,
                fontFamily: BODY_FONT
              },
              containerStyle: {
                backgroundColor: SECONDARY_COLOR
              }
            }}
            rightIcon={{ name: "info" }}
            title={item.start + " to " + item.end}
            titleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
          />
        ))}
      </View>
    );
  }
}

class ErrorList extends React.Component {
  render() {
    return (
      <View>
        {errorsList.map((item, i) => (
          <ListItem
            key={i}
            badge={{
              value: item.day,
              textStyle: {
                color: FONT_SECONDARY_COLOR,
                fontFamily: BODY_FONT
              },
              containerStyle: {
                backgroundColor: SECONDARY_COLOR
              }
            }}
            rightIcon={{ name: "info" }}
            title={item.name}
            titleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
            subtitleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
            subtitle={item.time}
          />
        ))}
      </View>
    );
  }
}

@inject("store")
@observer
export default class HomeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: PRIMARY_VARIENT_COLOR
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontFamily: BODY_FONT
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      devicesTabSelected: true,
      schedulesTabSelected: false,
      errorsTabSelected: false,
      devices: 0
    };
    this.devicesTabPressed = this.devicesTabPressed.bind(this);
    this.schedulesTabPressed = this.schedulesTabPressed.bind(this);
    this.errorsTabPressed = this.errorsTabPressed.bind(this);
  }

  componentDidMount(){
    this.handle = setInterval(() => {
      getRealTimeDevices(this.props.store);
    }, 5000);
  }

  _onRefresh = () => {
    getDevices(this.props.store);
    getRealTimeDevices(this.props.store);
  }

  devicesTabPressed() {
    this.props.store.ui.isLoading = true;
    this.setState({
      devicesTabSelected: true,
      schedulesTabSelected: false,
      errorsTabSelected: false
    });
    this.props.store.ui.isLoading = false;
  }

  schedulesTabPressed() {
    this.props.store.ui.isLoading = true;
    this.setState({
      devicesTabSelected: false,
      schedulesTabSelected: true,
      errorsTabSelected: false
    });
    this.props.store.ui.isLoading = false;
  }

  errorsTabPressed() {
    this.props.store.ui.isLoading = true;
    this.setState({
      devicesTabSelected: false,
      schedulesTabSelected: false,
      errorsTabSelected: true
    });
    this.props.store.ui.isLoading = false;
  }

  render() {
    const { store, navigation } = this.props;
    let list = "";
    if (this.state.devicesTabSelected) {
      list = <DeviceList />;
    } else if (this.state.schedulesTabSelected) {
      list = <ScheduleList />;
    } else {
      list = <ErrorList />;
    }

    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          refreshControl={
            <RefreshControl
              refreshing={store.ui.isLoading}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Card containerStyle={{ padding: 0 }}>
            <Grid>
              <Row style={styles.rowStyle}>
                <Col size={5}>
                  <Text style={styles.cardTitle}>Real Time</Text>
                  <Text style={styles.cardSubTitle}>Device Active Now</Text>
                </Col>
                <Col size={1}>
                  <Entypo name="share" size={30} color={PRIMARY_VARIENT_COLOR} />
                </Col>
                <Col size={1}>
                  <Entypo name="dots-three-vertical" size={30} color={PRIMARY_VARIENT_COLOR} />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text
                    style={{
                      textAlign: "center",
                      fontFamily: TITLE_FONT,
                      fontSize: 32,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    {store.device.live}
                  </Text>
                </Col>
              </Row>
            </Grid>
          </Card>
          <Card containerStyle={{ padding: 0 }}>
            <Grid>
              <Row style={styles.rowStyle}>
                <Col>
                  <Text style={styles.cardTitle}>iFill Overview</Text>
                  <Text style={styles.cardSubTitle}>Analytics of your devices</Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <TouchableOpacity onPress={this.devicesTabPressed}>
                    <View
                      style={{
                        backgroundColor: this.state.devicesTabSelected
                          ? PRIMARY_COLOR
                          : BACKGROUND_COLOR,
                        height: 100
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          fontFamily: BODY_FONT,
                          fontSize: 16,
                          alignItems: "center",
                          justifyContent: "center",
                          color: this.state.devicesTabSelected
                            ? BACKGROUND_COLOR
                            : DEFAULT_TEXT_COLOR
                        }}
                      >
                        {"\n"}
                        Devices
                      </Text>
                      <Text
                        style={{
                          textAlign: "center",
                          fontFamily: TITLE_FONT,
                          fontSize: 32,
                          alignItems: "center",
                          justifyContent: "center",
                          color: this.state.devicesTabSelected
                            ? BACKGROUND_COLOR
                            : DEFAULT_TEXT_COLOR
                        }}
                      >
                        {store.device.length}{"\n"}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </Col>
                <Col>
                  <TouchableOpacity onPress={this.schedulesTabPressed}>
                    <View
                      style={{
                        backgroundColor: this.state.schedulesTabSelected
                          ? PRIMARY_COLOR
                          : BACKGROUND_COLOR,
                        height: 100
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          fontFamily: BODY_FONT,
                          fontSize: 16,
                          alignItems: "center",
                          justifyContent: "center",
                          color: this.state.schedulesTabSelected
                            ? BACKGROUND_COLOR
                            : DEFAULT_TEXT_COLOR
                        }}
                      >
                        {"\n"}
                        Schedules
                      </Text>
                      <Text
                        style={{
                          textAlign: "center",
                          fontFamily: TITLE_FONT,
                          fontSize: 32,
                          alignItems: "center",
                          justifyContent: "center",
                          color: this.state.schedulesTabSelected
                            ? BACKGROUND_COLOR
                            : DEFAULT_TEXT_COLOR
                        }}
                      >
                        2{"\n"}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </Col>
                <Col>
                  <TouchableOpacity onPress={this.errorsTabPressed}>
                    <View
                      style={{
                        backgroundColor: this.state.errorsTabSelected
                          ? PRIMARY_COLOR
                          : BACKGROUND_COLOR,
                        height: 100
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          fontFamily: BODY_FONT,
                          fontSize: 16,
                          alignItems: "center",
                          justifyContent: "center",
                          color: this.state.errorsTabSelected
                            ? BACKGROUND_COLOR
                            : DEFAULT_TEXT_COLOR
                        }}
                      >
                        {"\n"}
                        Errors
                      </Text>
                      <Text
                        style={{
                          textAlign: "center",
                          fontFamily: TITLE_FONT,
                          fontSize: 32,
                          alignItems: "center",
                          justifyContent: "center",
                          color: this.state.errorsTabSelected
                            ? BACKGROUND_COLOR
                            : DEFAULT_TEXT_COLOR
                        }}
                      >
                        2{"\n"}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row>
                <Col>
                  {list}
                </Col>
              </Row>
            </Grid>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  cardTitle: {
    fontFamily: TITLE_FONT,
    fontSize: 20
  },
  cardSubTitle: {
    fontFamily: BODY_FONT,
    fontSize: 16
  },
  rowStyle: {
    borderBottomColor: "black",
    borderBottomWidth: 1,
    padding: 10
  },
  cardTab: {
    textAlign: "center",
    fontFamily: BODY_FONT,
    fontSize: 18,
    alignSelf: "center",
    justifyContent: "center"
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    paddingTop: 30
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: "rgba(96,100,109, 0.8)"
  },
  codeHighlightContainer: {
    backgroundColor: "rgba(0,0,0,0.05)",
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    lineHeight: 24,
    textAlign: "center"
  },
  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: "center",
    backgroundColor: "#fbfbfb",
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    textAlign: "center"
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: "center"
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: "#2e78b7"
  }
});
