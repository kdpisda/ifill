import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView,
  Image,
  Button,
  Alert
} from "react-native";
import { Card } from "react-native-elements";
import { inject } from "mobx-react";
import { observer } from "mobx-react/native";
import { signIn } from "../utils";
// import Button from "../components/button/Button";
import { BACKGROUND_COLOR, BODY_FONT } from "../config";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    backgroundColor: BACKGROUND_COLOR
  },
  loginCard: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  inputBox: {
    padding: 10,
    borderColor: "#ddd",
    borderWidth: 1,
    width: "100%",
    height: 50,
    margin: 5,
    alignSelf: "center",
    textAlign: "center",
    fontFamily: BODY_FONT,
    fontSize: 20
  },
  button: {
    padding: 0,
    borderColor: "#ddd",
    borderWidth: 1,
    width: 275,
    height: 50,
    margin: 5,
    alignSelf: "center"
  },
  cardHeader: {
    fontFamily: BODY_FONT,
    fontSize: 18,
    textAlign: "center"
  },
  bottomText: {
    fontFamily: BODY_FONT
  },
  bottomTextLink: {
    fontFamily: BODY_FONT,
    textDecorationLine: "underline"
  }
});

@inject("store")
@observer
class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    const { store, navigation } = this.props;
    if (store.auth.jwt !== "guesttoken" && store.auth.jwt != null) {
      navigation.navigate("Main");
    }
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  };

  state = {
    username: "",
    password: ""
  };

  render() {
    const { username, password } = this.state;
    const { store, navigation } = this.props;
    return (
      <KeyboardAvoidingView
        behavior="padding"
        style={{
          flex: 1
        }}
      >
        <View style={styles.container}>
          <Image
            style={{ width: 275, height: 150 }}
            source={require("../assets/images/icon.png")}
          />
          <Card style={styles.loginCard}>
            <View
              style={{
                width: 275,
                alignSelf: "center",
                justifyContent: "center",
                borderRadius: 20
              }}
            >
              <Text style={styles.cardHeader}>Login {"\n"}</Text>
              <TextInput
                style={styles.inputBox}
                underlineColorAndroid="transparent"
                placeholder="Email"
                onChangeText={text => this.setState({ username: text })}
              />
              <TextInput
                style={styles.inputBox}
                secureTextEntry
                underlineColorAndroid="transparent"
                placeholder="Password"
                onChangeText={text => this.setState({ password: text })}
              />
              <Button
                loading={store.ui.isLoading}
                title="Sign In"
                margin={30}
                onPress={async () => {
                  if (!this.validateEmail(username)) {
                    Alert.alert(
                      'Invalid Email',
                      'Please enter a valid email',
                      [
                        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                    )
                  } else {
                    const auth = await signIn(store, username, password);
                    if (auth) {
                      navigation.navigate("Main");
                    }else{
                      Alert.alert(
                        'Login Failed',
                        'Sorry an error occured, make sure you have entered right email and password',
                        [
                          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                    }
                  }
                }}
              />
            </View>
          </Card>
          <Text>{"\n"}</Text>
          <Text style={styles.bottomText}> Don't have an account yet ?</Text>
          <Text style={styles.bottomTextLink} onPress={() => navigation.navigate("Signup")}>
            Sign Up
          </Text>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default LoginScreen;
