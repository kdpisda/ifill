import React from 'react';
import { ScrollView, StyleSheet, Text, View, RefreshControl, TouchableOpacity } from 'react-native';
import { PRIMARY_VARIENT_COLOR, BODY_FONT, FONT_PRIMARY_COLOR, PRIMARY_COLOR, DEFAULT_TEXT_COLOR, TITLE_FONT, SECONDARY_COLOR, FONT_SECONDARY_COLOR, SECONDARY_VARIENT_COLOR } from '../../config';
import { Card, Badge } from 'react-native-elements';
import { inject } from "mobx-react";
import { BarIndicator } from "react-native-indicators";
import { observer } from "mobx-react/native";
import { getDevices, requestToToggle } from '../../utils';
import { Grid, Row, Col } from 'react-native-easy-grid';

@inject("store")
@observer
export default class DeviceScreen extends React.Component {
  static navigationOptions = {
    headerTitle: 'Devices',
    headerStyle:{
      backgroundColor: PRIMARY_VARIENT_COLOR
    },
    headerTintColor: '#fff',
    headerTitleStyle:{
      fontFamily: BODY_FONT
    },
  }

  state = {
    interval: null
  }

  componentDidMount () {
    this.handle = setInterval(() => {
      getDevices(this.props.store)
    }, 5000);
  }

  componentWillUnmount () {
    clearInterval(this.handle);
  }

  _onRefresh = () => {
    getDevices(this.props.store);
  }

  render() {
    const { store, navigation } = this.props;
    
    return (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={store.ui.isLoading}
            onRefresh={this._onRefresh}
          />
        }
      >
        {
          store.device.deviceList.map((item) => (
            <TouchableOpacity
              key={item.id}
              onPress={() => {
                if(requestToToggle(store, item.id)){
                  {this._onRefresh};
                }
              }}
            >
              <Card containerStyle={{padding: 0}}>
                <View style={{
                    backgroundColor: item.status ? PRIMARY_COLOR : SECONDARY_COLOR,
                    padding: 15
                  }}
                >
                  <Text style={{
                    color: item.status ? FONT_PRIMARY_COLOR : FONT_SECONDARY_COLOR,
                    fontFamily: TITLE_FONT,
                    fontSize: 32,
                    textAlign: 'center'
                  }}>
                    {item.name}
                  </Text>
                </View>
                <View style={{
                  backgroundColor: item.status ? PRIMARY_VARIENT_COLOR : SECONDARY_VARIENT_COLOR,
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: "center"
                }}>
                  <Grid>
                    <Row>
                      <Badge
                        containerStyle= {{
                          backgroundColor: item.status ? FONT_PRIMARY_COLOR : FONT_SECONDARY_COLOR
                        }}
                      >
                        <Text
                          style={{
                            color: item.status ? PRIMARY_COLOR : SECONDARY_COLOR,
                            fontFamily: BODY_FONT,
                            fontSize: 18,
                            textAlign: 'center'
                          }}
                        >
                          {item.status
                            ? 'Active '
                            : 'Inactive '
                          }
                          {item.percentage} % 
                        </Text>
                      </Badge>
                    </Row>
                    <Row style={{alignItems: "center", justifyContent: "center"}}>
                      {
                        item.status
                          ? <BarIndicator count={5} color={FONT_PRIMARY_COLOR}  />
                          : <Text style={{
                              fontFamily: BODY_FONT,
                              fontSize: 18,
                              textAlign: 'center'
                            }}>
                              Not Filling
                            </Text>
                      }
                    </Row>
                  </Grid>
                </View>
              </Card>
            </TouchableOpacity>
          ))
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  cardContentView: {
    backgroundColor: PRIMARY_COLOR,
    padding: 15
  },
  cardContent: {
    color: FONT_PRIMARY_COLOR,
    fontFamily: TITLE_FONT,
    fontSize: 32,
    textAlign: 'center'
  },
  cardFooter: {
    backgroundColor: PRIMARY_VARIENT_COLOR,
    color: FONT_PRIMARY_COLOR,
    fontFamily: BODY_FONT,
    textAlign: 'center'
  },
  cardFooterView: {
    backgroundColor: PRIMARY_VARIENT_COLOR,
    padding: 15,
    justifyContent: 'center',
    alignItems: "center"
  }
});
