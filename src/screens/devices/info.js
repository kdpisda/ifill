import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { Card } from "react-native-elements";
import Entypo from "@expo/vector-icons/Entypo";
import {
  PRIMARY_VARIENT_COLOR,
  BODY_FONT,
  FONT_PRIMARY_COLOR,
  PRIMARY_COLOR,
  TITLE_FONT
} from "../../config";

export default class DeviceScreen extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.navigation.state);
  }

  // static navigationOptions = {
  //   header: { visible: true },
  //   headerTitle: "Devices",
  //   headerStyle:{
  //     backgroundColor: PRIMARY_VARIENT_COLOR
  //   },
  //   headerTintColor: "#fff",
  //   headerTitleStyle:{
  //     fontFamily: BODY_FONT
  //   }
  // }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Card containerStyle={{ padding: 0 }}>
          <View
            style={{
              backgroundColor: PRIMARY_COLOR,
              padding: 15
            }}
          >
            <Text
              style={{
                color: FONT_PRIMARY_COLOR,
                fontFamily: TITLE_FONT,
                fontSize: 32,
                textAlign: "center"
              }}
            >
              Name
            </Text>
            <Text
              style={{
                color: FONT_PRIMARY_COLOR,
                fontFamily: TITLE_FONT,
                fontSize: 18,
                textAlign: "center"
              }}
            >
              Active
            </Text>
          </View>
          <View
            style={{
              backgroundColor: PRIMARY_VARIENT_COLOR,
              padding: 15,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Entypo name="info" color={FONT_PRIMARY_COLOR} />
          </View>
        </Card>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  cardContentView: {
    backgroundColor: PRIMARY_COLOR,
    padding: 15
  },
  cardContent: {
    color: FONT_PRIMARY_COLOR,
    fontFamily: TITLE_FONT,
    fontSize: 32,
    textAlign: "center"
  },
  cardFooter: {
    backgroundColor: PRIMARY_VARIENT_COLOR,
    color: FONT_PRIMARY_COLOR,
    fontFamily: BODY_FONT,
    textAlign: "center"
  },
  cardFooterView: {
    backgroundColor: PRIMARY_VARIENT_COLOR,
    padding: 15,
    justifyContent: "center",
    alignItems: "center"
  }
});
