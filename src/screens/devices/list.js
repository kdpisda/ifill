import React from "react";
import { View } from "react-native";
import { ListItem } from "react-native-elements";
import { inject } from "mobx-react";
import { observer } from "mobx-react/native";
import { getDevices } from "../../utils";

@inject("store")
@observer
export default class DeviceList extends React.Component {
  render() {
    const { store, navigation } = this.props;
    
    return (
      <View>
        {store.device.deviceList.map((item, i) => (
          <ListItem
            key={i}
            badge={{
              value: item.status ? "Active" : "Inactive",
              textStyle: {
                color: item.status ? FONT_SECONDARY_COLOR : FONT_ERROR_COLOR,
                fontFamily: BODY_FONT
              },
              containerStyle: {
                backgroundColor: item.status ? SECONDARY_COLOR : ERROR_COLOR
              }
            }}
            rightIcon={{ name: "info" }}
            title={item.name}
            subtitle={item.serial}
            titleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
            subtitleStyle={{ color: DEFAULT_TEXT_COLOR, fontFamily: BODY_FONT }}
          />
        ))}
    </View>
    );
  }
}