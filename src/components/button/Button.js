import React from "react";
import { Button } from "react-native-elements";
import { PRIMARY_COLOR, BODY_FONT } from "../../config";

const ButtonComponent = props => {
  const { title, color, bgcolor, width, height, margin, onPress, loading } = props;
  return (
    <Button
      loading={loading}
      onPress={onPress}
      title={title || "Button"}
      titleStyle={{
        fontFamily: BODY_FONT,
        fontWeight: "400",
        color: color || "#fff"
      }}
      buttonStyle={{
        backgroundColor: bgcolor || PRIMARY_COLOR,
        width: width || 250,
        height: height || 50,
        borderColor: "#ddd",
        borderWidth: 1,
        margin: margin || 0,
        padding: 5,
        elevation: 0
      }}
    />
  );
};

export default ButtonComponent;
